

//Constructor function for Person objects

// function Person (first, last, dob){
//     this.Vardas = first;
//     this.Pavarde = last;
//     this.Bday = new Date(dob);
// }
//  Person.prototype.getBirthYear = function () {
//      return this.Bday.getFullYear();
//  };

//  Person.prototype.getFullName = function(){
//      return `${this.Vardas}`;
//  };


// Class
class Person {
    constructor(first, last, dob) {
        this.Vardas = first;
        this.Pavarde = last;
        this.Bday = new Date(dob);
    }
    getBirthYear() {
        return this.Bday.getFullYear();
    }
    getFullName(){
        return `${this.Vardas} ${this.Pavarde}`;
    }
}


 const person1 = new Person ("Mantas", "Andreikevicius", "10-15-1990");
 const person2 = new Person ("Vidas", "Jurgas", "05-03-1998");
 
 console.log(person1.getBirthYear());
 console.log(person2.getFullName());





  